package mediscreen.front;

import feign.FeignException;
import mediscreen.front.dal.client.PatientApiClient;
import mediscreen.front.domain.model.Patient;
import mediscreen.front.web.controller.PatientController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.Collections;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(controllers = PatientController.class)
public class PatientControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PatientApiClient patientApiClient;

    @Test
    public void should_returnPatientList_whenGetPatientList() throws Exception {
        when(patientApiClient.getPatients()).thenReturn(Collections.singletonList(getFakePatient()));

        mockMvc.perform(get("/patient"))
                .andExpect(status().isOk())
                .andExpect(view().name("/patient/list"))
                .andExpect(content().string(containsString(String.valueOf(getFakePatient().getFamily()))));
    }

    @Test
    public void should_returnPatientAdd_whenGetPatientAdd() throws Exception {
        mockMvc.perform(get("/patient/add"))
                .andExpect(status().isOk())
                .andExpect(view().name("/patient/add"));
    }

    @Test
    public void should_returnBindingErrors_whenPostPartialPatientAdd() throws Exception {
        Patient patientPartial = getFakePatient();
        patientPartial.setFamily("");

        mockMvc.perform(post("/patient/add")
                        .flashAttr("patient", patientPartial))
                .andExpect(status().isOk())
                .andExpect(model().attributeHasErrors("patient"))
                .andExpect(view().name("/patient/add"));
    }

    @Test
    public void should_createPatient_whenPostPatientAdd() throws Exception {
        Patient patient = getFakePatient();

        mockMvc.perform(post("/patient/add")
                        .flashAttr("patient", patient))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/patient"));
    }

    @Test
    public void should_returnPatientUpdate_whenGetExistingPatientUpdate() throws Exception {
        when(patientApiClient.getPatientById(anyInt())).thenReturn(getFakePatient());

        mockMvc.perform(get("/patient/update/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("/patient/update"));
    }

    @Test
    public void should_notReturnPatientUpdate_whenGetMissingPatientUpdate() throws Exception {
        when(patientApiClient.getPatientById(anyInt())).thenThrow(FeignException.class);

        mockMvc.perform(get("/patient/update/1"))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/patient"));
    }

    @Test
    public void should_returnBindingErrors_whenPostPartialPatientUpdate() throws Exception {
        Patient patientPartial = getFakePatient();
        patientPartial.setFamily("");

        mockMvc.perform(post("/patient/update/1")
                        .flashAttr("patient", patientPartial))
                .andExpect(status().isOk())
                .andExpect(model().attributeHasErrors("patient"))
                .andExpect(view().name("/patient/update"));
    }

    @Test
    public void should_updatePatient_whenPostExistingPatientUpdate() throws Exception {
        Patient patient = getFakePatient();

        mockMvc.perform(post("/patient/update/1")
                        .flashAttr("patient", patient))
                .andExpect(status().isOk())
                .andExpect(view().name("/patient/update"));
    }

    @Test
    public void should_notUpdatePatient_whenPostMissingPatientUpdate() throws Exception {
        Patient patient = getFakePatient();
        doThrow(FeignException.class).when(patientApiClient).updatePatient(anyInt(), any(Patient.class));

        mockMvc.perform(post("/patient/update/1")
                        .flashAttr("patient", patient))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/patient"));
    }

    @Test
    public void should_deletePatient_whenPostExistingPatientDelete() throws Exception {
        mockMvc.perform(get("/patient/delete/1"))
                .andExpect(status().isFound())
                .andExpect(MockMvcResultMatchers.flash().attribute("rightDeletedPatient", true))
                .andExpect(view().name("redirect:/patient"));
    }

    @Test
    public void should_notDeletePatient_whenPostMissingPatientDelete() throws Exception {
        doThrow(FeignException.class).when(patientApiClient).deletePatientById(anyInt());
        mockMvc.perform(get("/patient/delete/1"))
                .andExpect(status().isFound())
                .andExpect(MockMvcResultMatchers.flash().attribute("missingPatientId", true))
                .andExpect(view().name("redirect:/patient"));
    }

    private Patient getFakePatient() {
        Patient patient = new Patient();
        patient.setId(1);
        patient.setFamily("FamilyTest");
        patient.setGiven("GivenTest");
        patient.setDob(LocalDate.of(2001, 1, 1));
        patient.setSex("M");
        patient.setAddress("AdressTest");
        patient.setPhone("PhoneTest");
        return patient;
    }


}
