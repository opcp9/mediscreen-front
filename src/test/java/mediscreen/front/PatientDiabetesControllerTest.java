package mediscreen.front;

import feign.FeignException;
import mediscreen.front.dal.client.PatientDiabetesApiClient;
import mediscreen.front.domain.model.Patient;
import mediscreen.front.domain.model.PatientDiabetesReport;
import mediscreen.front.web.controller.PatientDiabetesController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(controllers = PatientDiabetesController.class)
public class PatientDiabetesControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PatientDiabetesApiClient patientDiabetesApiClient;

    @Test
    void should_returnPatientDiabetesReport_whenGetPatientDiabetesReportByExistingPatientId() throws Exception {
        when(patientDiabetesApiClient.getPatientDiabetesReportByPatientId(anyInt())).thenReturn(getFakePatientDiabetesReport());

        mockMvc.perform(get("/patientdiabetes?patientId=1"))
                .andExpect(status().isOk())
                .andExpect(view().name("/patientdiabetes/report"));
    }

    @Test
    void should_notReturnPatientDiabetesReport_whenGetPatientDiabetesReportByMissingPatientId() throws Exception {
        when(patientDiabetesApiClient.getPatientDiabetesReportByPatientId(anyInt())).thenThrow(FeignException.class);

        mockMvc.perform(get("/patientdiabetes?patientId=1"))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/patient"));
    }

    private PatientDiabetesReport getFakePatientDiabetesReport() {
        PatientDiabetesReport patientDiabetesReport = new PatientDiabetesReport();
        Patient patient = new Patient();
        patient.setId(1);
        patient.setFamily("FamilyTest");
        patient.setGiven("GivenTest");
        patient.setDob(LocalDate.of(2000, 1, 1));
        patient.setSex("F");
        patient.setAddress("AddressTest");
        patient.setPhone("PhoneTest");
        patientDiabetesReport.setPatient(patient);
        patientDiabetesReport.setPatientDiabetesRating("Borderline");
        return patientDiabetesReport;
    }

}
