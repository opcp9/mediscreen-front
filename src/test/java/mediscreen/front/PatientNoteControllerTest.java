package mediscreen.front;

import feign.FeignException;
import mediscreen.front.dal.client.PatientNoteApiClient;
import mediscreen.front.domain.model.PatientNote;
import mediscreen.front.web.controller.PatientNoteController;
import mediscreen.front.web.dto.PatientBreadCrumb;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Collections;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(controllers = PatientNoteController.class)
public class PatientNoteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PatientNoteApiClient patientNoteApiClient;

    @Test
    public void should_returnPatientNoteList_whenGetPatientNoteList() throws Exception {
        when(patientNoteApiClient.getPatientNotesByPatientId(anyInt())).thenReturn(Collections.singletonList(getFakePatientNote()));

        mockMvc.perform(get("/patientnote/list/patient/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("/patientnote/list"))
                .andExpect(content().string(containsString(String.valueOf(getFakePatientNote().getPatientNote()))));
    }

    @Test
    public void should_returnPatientNoteAdd_whenGetPatientNoteAdd() throws Exception {
        mockMvc.perform(get("/patientnote/add"))
                .andExpect(status().isOk())
                .andExpect(view().name("/patientnote/add"));
    }

    @Test
    public void should_createPatientNote_whenPostPatientNoteAdd() throws Exception {
        PatientNote patientNote = getFakePatientNote();

        mockMvc.perform(post("/patientnote/add")
                        .flashAttr("patientNote", patientNote))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/patientnote/list/patient/1"));
    }

    @Test
    public void should_returnPatientNoteUpdate_whenGetExistingPatientNoteUpdate() throws Exception {
        when(patientNoteApiClient.getPatientNoteById(anyString())).thenReturn(getFakePatientNote());

        mockMvc.perform(get("/patientnote/update/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("/patientnote/update"));
    }

    @Test
    public void should_notReturnPatientNoteUpdate_whenGetMissingPatientNoteUpdate() throws Exception {
        when(patientNoteApiClient.getPatientNoteById(anyString())).thenThrow(FeignException.class);

        mockMvc.perform(get("/patientnote/update/1")
                        .flashAttr("patientBreadCrumb", new PatientBreadCrumb(1)))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/patientnote/list/patient/1"));
    }

    @Test
    public void should_updatePatientNote_whenPostExistingPatientNoteUpdate() throws Exception {
        PatientNote patientNote = getFakePatientNote();

        mockMvc.perform(post("/patientnote/update/1")
                        .flashAttr("patientNote", patientNote))
                .andExpect(status().isOk())
                .andExpect(view().name("/patientnote/update"));
    }

    @Test
    public void should_notUpdatePatientNote_whenPostMissingPatientNoteUpdate() throws Exception {
        PatientNote patientNote = getFakePatientNote();
        doThrow(FeignException.class).when(patientNoteApiClient).updatePatientNote(anyString(), any(PatientNote.class));

        mockMvc.perform(post("/patientnote/update/1")
                        .flashAttr("patientNote", patientNote)
                        .flashAttr("patientBreadCrumb", new PatientBreadCrumb(1)))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/patientnote/list/patient/1"));
    }

    @Test
    public void should_deletePatientNote_whenPostExistingPatientNoteDelete() throws Exception {
        mockMvc.perform(get("/patientnote/delete/1")
                        .flashAttr("patientBreadCrumb", new PatientBreadCrumb(1)))
                .andExpect(status().isFound())
                .andExpect(MockMvcResultMatchers.flash().attribute("rightDeletedPatientNote", true))
                .andExpect(view().name("redirect:/patientnote/list/patient/1"));
    }

    @Test
    public void should_notDeletePatientNote_whenPostMissingPatientNoteDelete() throws Exception {
        doThrow(FeignException.class).when(patientNoteApiClient).deletePatientNoteById(anyString());
        mockMvc.perform(get("/patientnote/delete/1")
                        .flashAttr("patientBreadCrumb", new PatientBreadCrumb(1)))
                .andExpect(status().isFound())
                .andExpect(MockMvcResultMatchers.flash().attribute("missingPatientNoteId", true))
                .andExpect(view().name("redirect:/patientnote/list/patient/1"));
    }

    private PatientNote getFakePatientNote() {
        PatientNote patientNote = new PatientNote();
        patientNote.setId("1");
        patientNote.setPatientId(1);
        patientNote.setPatientNote("PatientNoteTest");
        return patientNote;
    }


}
