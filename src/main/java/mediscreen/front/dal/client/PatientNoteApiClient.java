package mediscreen.front.dal.client;

import feign.Headers;
import mediscreen.front.domain.model.PatientNote;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(value = "PatientNoteApiClient", url = "${clients.patientnoteapiclienturl}", path = "/patientnote")
public interface PatientNoteApiClient {

    @GetMapping("/patient/{patientid}")
    List<PatientNote> getPatientNotesByPatientId(@PathVariable("patientid") Integer patientId);

    @GetMapping("{id}")
    PatientNote getPatientNoteById(@PathVariable("id") String id);

    @PostMapping()
    @Headers("Content-Type: application/json")
    void createPatient(@RequestBody PatientNote patientNote);

    @PutMapping("/{id}")
    @Headers("Content-Type: application/json")
    void updatePatientNote(@PathVariable("id") String id, @RequestBody PatientNote patientNote);

    @DeleteMapping("/{id}")
    void deletePatientNoteById(@PathVariable("id") String id);
}
