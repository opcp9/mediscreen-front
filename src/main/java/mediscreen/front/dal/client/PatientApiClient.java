package mediscreen.front.dal.client;

import feign.Headers;
import mediscreen.front.domain.model.Patient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(value = "PatientApiClient", url = "${clients.patientapiclienturl}", path = "/patient")
public interface PatientApiClient {
    @GetMapping()
    List<Patient> getPatients();

    @GetMapping("/{id}")
    Patient getPatientById(@PathVariable("id") Integer id);

    @PostMapping()
    @Headers("Content-Type: application/json")
    void createPatient(Patient patient);

    @PutMapping("{id}")
    @Headers("Content-Type: application/json")
    void updatePatient(@PathVariable("id") Integer id, @RequestBody Patient patient);

    @DeleteMapping("{id}")
    void deletePatientById(@PathVariable("id") Integer id);
}
