package mediscreen.front.dal.client;

import mediscreen.front.domain.model.PatientDiabetesReport;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "PatientDiabetesApiClient", url = "${clients.patientdiabetesapiclienturl}", path = "/patientdiabetes")
public interface PatientDiabetesApiClient {

    @GetMapping()
    PatientDiabetesReport getPatientDiabetesReportByPatientId(@RequestParam("patientId") Integer patientId);
}
