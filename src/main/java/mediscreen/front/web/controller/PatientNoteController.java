package mediscreen.front.web.controller;

import feign.FeignException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mediscreen.front.dal.client.PatientNoteApiClient;
import mediscreen.front.domain.model.PatientNote;
import mediscreen.front.web.dto.PatientBreadCrumb;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping("/patientnote")
public class PatientNoteController {

    private final PatientNoteApiClient patientNoteApiClient;

    @GetMapping("/list/patient/{patientId}")
    public String getPatientNotesByPatientId(@PathVariable Integer patientId,
                                             Model model) {
        log.debug("request for displaying patient note list of patientId={}", patientId);

        model.addAttribute("patientNotes", patientNoteApiClient.getPatientNotesByPatientId(patientId));
        model.addAttribute("patientBreadCrumb", new PatientBreadCrumb(patientId));
        return "/patientnote/list";
    }

    @GetMapping("/add")
    public String getPatientNoteAdd(@ModelAttribute PatientBreadCrumb patientBreadCrumb,
                                    Model model) {
        log.debug("request for displaying patient note add");

        PatientNote patientNote = new PatientNote();
        patientNote.setPatientId(patientBreadCrumb.getPatientId());
        model.addAttribute("patientNote", patientNote);
        return "/patientnote/add";
    }

    @PostMapping("/add")
    public String postPatientNoteAdd(@ModelAttribute PatientNote patientNote,
                                     RedirectAttributes redirectAttributes) {
        log.debug("request for posting patient note add");

        patientNoteApiClient.createPatient(patientNote);
        redirectAttributes.addFlashAttribute("rightCreatedPatientNote", true);
        return "redirect:/patientnote/list/patient/" + patientNote.getPatientId();
    }

    @GetMapping("/update/{id}")
    public String getPatientNoteUpdate(@PathVariable("id") String id,
                                       @ModelAttribute PatientBreadCrumb patientBreadCrumb,
                                       Model model,
                                       RedirectAttributes redirectAttributes) {
        log.debug("request for displaying patient note update with id={}", id);

        try {
            PatientNote patientNote = patientNoteApiClient.getPatientNoteById(id);
            model.addAttribute("patientNote", patientNote);
            return "/patientnote/update";
        } catch (FeignException e) {
            log.error("can't update missing patient note with id={}", id);
            redirectAttributes.addFlashAttribute("missingPatientNoteId", true);
            return "redirect:/patientnote/list/patient/" + patientBreadCrumb.getPatientId();
        }
    }

    @PostMapping("/update/{id}")
    public String postPatientNoteUpdate(@PathVariable("id") String id,
                                        @ModelAttribute PatientNote patientNote,
                                        @ModelAttribute PatientBreadCrumb patientBreadCrumb,
                                        Model model,
                                        RedirectAttributes redirectAttributes) {
        log.debug("request for posting patientNote update with id={}", id);

        try {
            patientNote.setId(id);
            patientNoteApiClient.updatePatientNote(patientNote.getId(), patientNote);
            model.addAttribute("rightUpdatedPatientNote", true);
            return "/patientnote/update";
        } catch (FeignException e) {
            log.error("can't update missing patientNote with id={}", id);
            redirectAttributes.addFlashAttribute("missingPatientNoteId", true);
            return "redirect:/patientnote/list/patient/" + patientBreadCrumb.getPatientId();
        }
    }

    @GetMapping("/delete/{id}")
    public String deletePatientNote(@PathVariable("id") String id,
                                    @ModelAttribute PatientBreadCrumb patientBreadCrumb,
                                    RedirectAttributes redirectAttributes) {
        log.debug("request for deleting patientNote with id={}", id);

        try {
            patientNoteApiClient.deletePatientNoteById(id);
            redirectAttributes.addFlashAttribute("rightDeletedPatientNote", true);
        } catch (FeignException e) {
            log.error("can't delete missing patientNote with id={}", id);
            redirectAttributes.addFlashAttribute("missingPatientNoteId", true);
        }
        return "redirect:/patientnote/list/patient/" + patientBreadCrumb.getPatientId();
    }

}
