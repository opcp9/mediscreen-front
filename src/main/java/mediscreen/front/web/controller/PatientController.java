package mediscreen.front.web.controller;

import feign.FeignException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mediscreen.front.dal.client.PatientApiClient;
import mediscreen.front.domain.model.Patient;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping("/patient")
public class PatientController {

    private final PatientApiClient patientApiClient;

    @GetMapping()
    public String getPatientList(Model model) {
        log.debug("request for displaying patient list page");

        model.addAttribute("patients", patientApiClient.getPatients());
        return "/patient/list";
    }

    @GetMapping("/add")
    public String getPatientAdd(Model model) {
        log.debug("request for displaying patient add page");

        model.addAttribute("patient", new Patient());
        return "/patient/add";
    }

    @PostMapping("/add")
    public String postPatientAdd(@Valid @ModelAttribute Patient patient,
                                 BindingResult result,
                                 RedirectAttributes redirectAttributes) {
        log.debug("request for posting patient add with family={}", patient.getFamily());

        if (result.hasErrors()) {
            return "/patient/add";
        }
        patientApiClient.createPatient(patient);
        redirectAttributes.addFlashAttribute("rightCreatedPatient", true);
        return "redirect:/patient";
    }

    @GetMapping("/update/{id}")
    public String getPatientUpdate(@PathVariable("id") Integer id,
                                   Model model,
                                   RedirectAttributes redirectAttributes) {
        log.debug("request for displaying patient update with id={}", id);

        try {
            Patient patient = patientApiClient.getPatientById(id);
            model.addAttribute("patient", patient);
            return "/patient/update";
        } catch (FeignException e) {
            log.error("can't update missing patient with id={}", id);
            redirectAttributes.addFlashAttribute("missingPatientId", true);
            return "redirect:/patient";
        }
    }

    @PostMapping("/update/{id}")
    public String postPatientUpdate(@PathVariable("id") Integer id,
                                    @Valid @ModelAttribute Patient patient,
                                    BindingResult result,
                                    Model model,
                                    RedirectAttributes redirectAttributes) {
        log.debug("request for posting patient update with id={}", id);

        if (result.hasErrors()) {
            return "/patient/update";
        }

        try {
            patient.setId(id);
            patientApiClient.updatePatient(patient.getId(), patient);
            model.addAttribute("rightUpdatedPatient", true);
            return "/patient/update";
        } catch (FeignException e) {
            log.error("can't update missing patient with id={}", id);
            redirectAttributes.addFlashAttribute("missingPatientId", true);
            return "redirect:/patient";
        }
    }

    @GetMapping("/delete/{id}")
    public String deletePatient(@PathVariable("id") Integer id,
                                RedirectAttributes redirectAttributes) {
        log.debug("request for deleting patient with id={}", id);

        try {
            patientApiClient.deletePatientById(id);
            redirectAttributes.addFlashAttribute("rightDeletedPatient", true);
        } catch (FeignException e) {
            log.error("can't delete missing patient with id={}", id);
            redirectAttributes.addFlashAttribute("missingPatientId", true);
        }
        return "redirect:/patient";
    }

}
