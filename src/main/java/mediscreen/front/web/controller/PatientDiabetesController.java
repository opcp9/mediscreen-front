package mediscreen.front.web.controller;

import feign.FeignException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mediscreen.front.dal.client.PatientDiabetesApiClient;
import mediscreen.front.domain.model.PatientDiabetesReport;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping("/patientdiabetes")
public class PatientDiabetesController {

    private final PatientDiabetesApiClient patientDiabetesApiClient;

    @GetMapping()
    public String getPatientDiabetesReportByPatientId(@RequestParam("patientId") Integer patientId,
                                                      Model model,
                                                      RedirectAttributes redirectAttributes) {
        log.debug("request for displaying patient diabetes report with id={}", patientId);

        try {
            PatientDiabetesReport patientDiabetesReport = patientDiabetesApiClient.getPatientDiabetesReportByPatientId(patientId);
            model.addAttribute("patientDiabetesReport", patientDiabetesReport);
            return "/patientdiabetes/report";
        } catch (FeignException e) {
            log.error("can't get diabetes report for missing patient with id={}", patientId);
            redirectAttributes.addFlashAttribute("missingPatientId", true);
            return "redirect:/patient";
        }
    }
}
