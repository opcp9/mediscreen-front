package mediscreen.front.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PatientBreadCrumb {

    public PatientBreadCrumb(Integer patientId) {
        this.patientId = patientId;
    }

    private Integer patientId;
}
